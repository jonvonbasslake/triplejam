﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public GameObject[] mainMenuScreens;

    public void LoadScene() { SceneManager.LoadScene("ArttuWorkshop"); }

    public void OpenCredits() { mainMenuScreens[0].SetActive(false); mainMenuScreens[1].SetActive(true);}

    public void BackToMainMenu() { mainMenuScreens[0].SetActive(true); mainMenuScreens[1].SetActive(false); }
}
