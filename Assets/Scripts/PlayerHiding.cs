﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Created by Arttu Paldán 24.11.2020: Simple script for player to hide himself behind an object.
public class PlayerHiding : MonoBehaviour
{
    Transform myTransform;

    bool canHide, isHiding;

    void Awake() { myTransform = GetComponent<Transform>(); }

    void OnTriggerEnter2D(Collider2D collision) { if (collision.CompareTag("HidingPlace")) { canHide = true; } }

    private void OnTriggerExit2D(Collider2D collision) { canHide = false; ComeOutOfHiding(); }

    void Update() { CheckInput(); CheckHiding(); }

    void CheckInput()
    {
        if (canHide && Input.GetKey(KeyCode.E)) { Hide(); }
        else if (isHiding && Input.GetKey(KeyCode.R)) { ComeOutOfHiding(); }
    }

    void CheckHiding() { if (isHiding) { } }

    void Hide() { isHiding = true; myTransform.position = new Vector3(myTransform.position.x, myTransform.position.y, 10); }
    void ComeOutOfHiding() { isHiding = false;  myTransform.position = new Vector3(myTransform.position.x, myTransform.position.y, 0); }

    public bool GetPlayerHiding() { return isHiding; }
}
