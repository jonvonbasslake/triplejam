﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Created by Arttu Paldán 24.11.2020: Simple movement script for the player. 
public class PlayerMovement : MonoBehaviour
{
    SoundLevel soundLevel;

    Transform myTransform;

    public float moveSpeed;

    void Awake() { myTransform = GetComponent<Transform>(); soundLevel = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SoundLevel>(); }

    void Update() { CheckInput(); }

    void CheckInput()
    {
        if (Input.GetKey(KeyCode.D)) { MovesRight(); }
        else if (Input.GetKey(KeyCode.A)) { MovesLeft(); }
        else { soundLevel.ItIsQuiet(); }
    }

    void MovesRight() { myTransform.Translate(Vector2.right * moveSpeed * Time.deltaTime);  soundLevel.IncreaseSoundLevel(); }
    void MovesLeft() { myTransform.Translate(Vector2.left * moveSpeed * Time.deltaTime);  soundLevel.IncreaseSoundLevel(); }
}
