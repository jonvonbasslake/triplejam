﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Created by Arttu Paldán on 24.11.2020: This basically tracks how much sound the player is making and delivers this information to KrambusBehavior script.  
public class SoundLevel : MonoBehaviour
{
    int soundsMadeByPlayer;

    public int increaseAmount;

    public void IncreaseSoundLevel() { soundsMadeByPlayer = soundsMadeByPlayer + increaseAmount; }

    public void ItIsQuiet() { soundsMadeByPlayer = 0; }

    public int GetSoundsMadeByPlayer() { return soundsMadeByPlayer; }
}
