﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum KrambusMode { None, Patrolling, Chasing, Attacking }

// Created by Arttu Paldán on 24.11.2020: Very basic AI for the KrambusBear.
public class KrambusBehavior : MonoBehaviour
{
    SoundLevel soundLevel;

    PlayerHiding playerHiding;

    [SerializeField] KrambusMode krambusMode;

    Transform myTransform, playersPosition;

    bool playerInSights, facingRight, playerDetected;

    int soundsMadeByPlayer;

    SpriteRenderer sprite;

    public float MoveSpeed, chaseSpeed, attackSpeed;

    public LayerMask playerLayer;

    void Awake() 
    { 
        myTransform = GetComponent<Transform>();
        playersPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        facingRight = true;
        soundLevel = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SoundLevel>();
        playerHiding = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHiding>();
        krambusMode = KrambusMode.None;
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update() { TrackSoundLevel(); IsPlayerInSight(); UpdateBearState(); KrambusDoesThings(); }

    void TrackSoundLevel() 
    { 
        soundsMadeByPlayer = soundLevel.GetSoundsMadeByPlayer();

        switch (soundsMadeByPlayer)
        {
            case 0:
                playerDetected = false;
                break;

            case 3000:
                playerDetected = true;
                break;
        }
    }

    void IsPlayerInSight()
    {
        Physics2D.Linecast(myTransform.position, myTransform.position * 2);
        Debug.DrawLine(myTransform.position, myTransform.position * 2, Color.red);

        if(Physics2D.Linecast(myTransform.position, myTransform.position * 5, playerLayer)) { playerInSights = true; }
        else { playerInSights = false; }
    }

    void UpdateBearState()
    {
        bool playerIsHiding = playerHiding.GetPlayerHiding();

        if (playerDetected) { krambusMode = KrambusMode.Chasing; }
        else if (playerInSights && playerIsHiding == false) { krambusMode = KrambusMode.Attacking; }
        else if (playerInSights == false) { krambusMode = KrambusMode.Patrolling; }
    }

    void KrambusDoesThings()
    {
        if(krambusMode == KrambusMode.Patrolling) { Patrol(); }
        else if(krambusMode == KrambusMode.Chasing) { Chase(); }
        else if(krambusMode == KrambusMode.Attacking) { Attack(); }
    }

    void Patrol()
    {
        if (facingRight) { myTransform.Translate(Vector2.right * MoveSpeed); }
        else { myTransform.Translate(Vector2.left * MoveSpeed); }
    }

    void Chase() { myTransform.position = Vector2.MoveTowards(transform.position, playersPosition.position, chaseSpeed); }

    void Attack() 
    {
        myTransform.position = Vector2.MoveTowards(transform.position, playersPosition.position, attackSpeed); 
        
        if(Vector2.Distance(transform.position, playersPosition.position) > 0) { Destroy(playersPosition.gameObject); }
    }

    void OnCollisionEnter2D(Collision2D collision) { if (collision.collider.CompareTag("Wall")) { FlipMe(); } }

    void FlipMe()
    {
        if (facingRight) { sprite.flipX = false; facingRight = false; }
        else { sprite.flipX = true; facingRight = true; }
    }
}
