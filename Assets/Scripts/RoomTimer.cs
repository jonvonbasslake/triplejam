﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Created by Arttu Paldán on 24.11.2020: Basic clock for to count time and make Krambus destroy hiding places. 
public class RoomTimer : MonoBehaviour
{
    Text clockText;

    float seconds, minutes;

    public List<GameObject> hidingPlaces;

    void Awake() { clockText = GetComponent<Text>(); }

    void Update() 
    {
        UpdateTime();
        DestroyHidingPlaces();
    }

    void UpdateTime()
    {
        minutes = (int)(Time.time / 60f);
        seconds = (int)(Time.time % 60f);

        clockText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }

    void DestroyHidingPlaces()
    {
        switch (minutes)
        {
            case 1:
                Destroy(hidingPlaces[0]);
                break;

            case 2:
                Destroy(hidingPlaces[1]);
                break;

            case 3:
                Destroy(hidingPlaces[2]);
                break;
        }
    }
}
